
fn find_index_of_element_in_vector(list: &Vec<i32>, target: i32, start: usize, end: usize, pass: i32) -> Result<usize, String> {
    let debug = false;

    if pass as u32 > (list.len() as u32).ilog2() + 1 {
        let error = String::from("Will not find.");
        return Err(error);
    }
    let middle: usize = start + ((end - start) / 2);
    if debug {
        let slice = &list[start..end];
        println!("{:?}", slice);
        println!("midde: {}", middle);
        println!("with value of: {}", list[middle]);
        println!("Pass: {}", pass);
    }
    if list[middle] == target{
        return Ok(middle);
    }
    else if list[middle] > target {
        return find_index_of_element_in_vector(list, target, start, middle - 1, pass + 1);
    }
    else if list[middle] < target {
        return find_index_of_element_in_vector(list, target, middle + 1 , end, pass + 1);
    }
    else {
        Err(String::from("Not found"))
    }
}


fn find_index_of_element(list: &[i32], target: i32, start: usize, end: usize, pass: i32) -> Result<usize, String> {
    let debug = false;

    if pass as u32 > (list.len() as u32).ilog2() + 1 {
        let error = String::from("Will not find.");
        return Err(error);
    }
    let middle: usize = start + ((end - start) / 2);
    if debug {
        let slice = &list[start..end];
        println!("{:?}", slice);
        println!("midde: {}", middle);
        println!("with value of: {}", list[middle]);
        println!("Pass: {}", pass);
    }
    if list[middle] == target{
        return Ok(middle);
    }
    else if list[middle] > target {
        return find_index_of_element(list, target, start, middle - 1, pass + 1);
    }
    else if list[middle] < target {
        return find_index_of_element(list, target, middle + 1 , end, pass + 1);
    }
    else {
        Err(String::from("Not found"))
    }
}


fn main() {
    let find: i32 = 52032;
    const SIZE: usize = 100_000_000;
    //let mut index: Result<usize, String> = Ok(0);

    /*if SIZE <= 1000000 {
        println!("How");
        let mut things: [i32; SIZE] = [0; SIZE];
        for x in 0..things.len() {
            things[x] = x as i32 * 2;
        }
        index = find_index_of_element(&things, find, 0, things.len(), 1);
    } else {
        
    }*/
    let mut things: Vec<i32> = Vec::new();
    for x in 0..SIZE {
        things.push(x as i32 * 2);
    }
    let index = find_index_of_element_in_vector(&things, find, 0, things.len(), 1);
    if index.is_ok(){
        println!("{} is the index of {}", index.unwrap(), find);
    }else {
        println!("coult not find {}", find)
        //println!("could not find {} in {:?}", find, things)
    }
}
