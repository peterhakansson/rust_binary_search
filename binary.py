from math import log2


def find(sorted_list, target, start, end, _pass):
    if _pass > log2(len(sorted_list)) + 1:
        return "Will not find."

    middle = start + ((end - start) // 2)

    if sorted_list[middle] == target:
        return middle

    elif sorted_list[middle] > target:
        return find(sorted_list, target, start, middle - 1, _pass + 1)
    elif sorted_list[middle] < target:
        return find(sorted_list, target, middle + 1, end, _pass + 1)
    else:
        return "What the"


SIZE = 100_000_000
things = []

for x in range(SIZE):
    things.append(x * 2)

target = 52032
index = find(things, target, 0, len(things), 1)

print(f"{index=}")
